This is an index of sorts of the questions that exist up through 2018. It gets a little weird for years before 2008:
- AMP used to be "Internal Processes" and "Interstellar Medium"
- RND apparently used to be split into a bunch of classes covering the 3 things which are in it now: dynamics, MHD, and radiation

Any question with a "*" attached to it means I thought it was a good question for study. If it has a "-" after it, it means I think it was not a useful question (i.e. it was overly focused on trivia or something like that). No mark doesn't mean the question is good or bad, I just didn't bother marking it. If a question description has "?" or "..." somewhere in it, that means I wasn't quite sure if I was characterizing the question correctly. 

This list should NOT be considered 100% accurate but it should get you started on finding practice questions.

-Eryn Cangi

FLUIDS
-------------------------------------------

1998 - Spiral inflow
1999 - Bernoulli/tanks and pipes
2000 - Geostrophic flow
2002 - Boussinesq, Linearization, instability
2003 - Tornado 
2004 - Gaussian vortex * 
2005 - Incompressibility, streamfunction?
2007 - Boundary layers
2008 - Geostrophic flow again
2009 - Jets: Navier-Stokes boundary layer, streamfunction
2010 - Boundary layers again
2011 - Surface gravity waves
2012 - Bondi Accretion
2013 - Shocks
2014 - Solar wind/opposite of Bondi accretion/Bernoulli "Xray source evaporates a planet"
2015 - Rotation/Bernoulli
2016 - Gaussian vortex again
2017 - Geostrophic flow again 
2018 - Fluid dynamical black hole, linearization, dispersion relation, vortex *

AMP
-------------------------------------------

1998 - Rotational emission, transitions in CO (old class title IP)
1999 - Accretion onto sun (IP)
1999 - HII regions, ionization (ISM)
2000 - Population densities and LTE in the solar photosphere (IP)
2001 - Stellar wind shocks (IP2)
2001 - 2 level atom, collisions (ISM) *
2001 - Spectroscopy, collisions, population ratios (IP) * 
2002 - Gas cooling (IP2)
2003 - Population densities, molecular cloud (IP)
2003 - Radiative cooling, thermal stability (ISM)
2004 - Shocks and radiative cooling (IP) *
2004 - Emission and absorption lines, curve of growth (ISM)
2005 - Radiation, equilibrium temperature of grains (ISM)
2005 - Planck, planet equilibrium temperature, rotational radiation (IP)
2006 - Kepler potentials and escape velocity (IP)
2006 - Radiative decay, collisions, diagnostics (ISM) *
2007 - Blackbodies and Planck (IP)
2007 - Specific stuff about ISM and HII regions (ISM) -
2008 - 21 cm radiation, collisions, redshift (IP1)
2008 - Bondi accretion (IP2)
2009 - Spectroscopy, line profiles, fine/hyperfine structure, Lamb shift
2010 - Molecular hydrogen: Rotational energy transitions, basic molecular spectroscopy, para/ortho
2011 - 2 level atom, critical density/low and high limits, LS coupling
2012 - Spectroscopy, Rotation transitions, fundamental constnats, Bohr model
2013 - Ionized and neutral H regions; spectroscopy; mean free path *
2014 - Deuterium, abundances, optical depth, line profiles
2015 - Emission and absorption processes, optically thin/thick approx, optical depth, lots of conceptual stuff about transitions 
2016 - Spectroscopy of Fe X, relative ionization populations in non-LTE, number densities *
2017 - Spectroscopy, forbidden and allowed transitions, collisions, critical densities *
2018 - "Positronium": energy compared to hydrogen, transitions, ortho/para, *

MATH METHODS
-------------------------------------------

1998 - Diffusion, homogeneous diffeq, Green's function - solutions are wrong (what they say cosh and sinh are is wrong)
1999 - Differential equations (chemical system)
2000 - Solving diffusion equation with Fourier
2003 - Waves, dispersion relation
2004 - Solving the critically damped oscillator, Green's function, communists
2005 - Solving some sort of weird diffeq they had to defend in the text as "it does make sense"
2006 - Heating/diffusion equation at planet's surface and into interior
2007 - Sturm-Liouville, eigenvector equation (sort of)
2008 - Heating/diffusion equation at planet's surface and into interior
2009 - Rotating fluids equations, Fourier solution to diffeqs, waves, dispersion relation
2010 - Fourier, Green's function
2011 - Acoustic waveguide; wave equation; modes; Fourier
2012 - Series solution to diffeq
2013 - Sound waves in stratified atmosphere
2014 - Separation of variables, orthogonality, integration over a small interval
2015 - Solving differential equations with calculus->algebra trick, eigenvectors and values, forward-in-time Taylor expansion
2016 - what is linear regression vs linear fitting?, linear regression with matrices, QR solution
2017 - Solving the fluids equations with matrices and dispersion relation, linear regression with matrices * 
2018 - Waves and dispersion relation, Fourier


OBS AND STATS
-------------------------------------------

1997 - Design a grating spectrograph, observations
1999 - CCDs, exposure times in observatinos, S/N
2000 - Diffraction limited telescope, resolution, focusing stuff, spectrograph
2005 - Signal to noise, DN, temperature, echelle spectrograph *
2006 - Flux, bandpasses, S/N, CCD noise, seeing *
2008 - Signal to noise, Poisson/photon counting, MLEs, curvature matrix, covariance
2009 - minimum chi-squared, variance and uncertainty
2010 - PSF, Rayleigh resolution, Fourier transform
2011 - Planck and telescope power
2012 - Photon counting devices, efficencies
2013 - Statistical significance of a signal
2014 - Telescope practical observing: efficiencies, signal-to-noise, radio telescopes *
2015 - Observations, binomial distribution, instrument resolution, photon counting and CCDs, S/N, chi squared statistic
2016 - Airy disk/Rayleigh resolution, magnitudes, S/N, confidence limits and Bayes theorem *
2017 - LIGO: Interferometry, error in experiments, Fourier transforms, noise
2018 - Linear regression with matrices, least squares; statistical estimator, uncertainty, converting electrons to flux, chi-by-eye *

R&D - newer stuff
-------------------------------------------

2010 - Solar corona, angular momentum, magnetized stellar wind...
2011 - Hill's equations, equations of motion
2012 - Hypervelocity stars: Hill radius, tidal forces, orbital dynamics
2013 - Magnetic fields vs. dynamical processes; Alfven; scaling ratios to compare processes
2014 - Particle motion in converging magnetic field; gyromotion; magnetic mirror
2015 - MHD: Induction equation, magnetic Reynolds number, Streamlines, basic diffeqs
2016 - Hypervelocity stars: Hill radius, tidal forces, orbital dynamics
2017 - effective potentials, oblate rotating things, gravity-darkening?
2018 - Tidal disruption event, tidal radius, orbital dynamics

Older R&D:

Dynamics problems - much of these is now part of Origins class, which is not core. If you've taken Origins it may improve your understanding of this content, but it's probably safe to ignore these questions. (Note: I am not responsible if you fail a question!)

1999 - Dynamics: forming the moon; orbits, mass, Keplerian potentials, tidal effects
2000 - Disk instabilities, dispersion relation, Toomre Q
2002 - GMC freefall, Type I migration, basically all Origins content
2003 - Exoplanet detection stuff 
2005 - Gas disk dispersion relation, Origins stuff
2006 - Collisional cross section, planet mass growth
2007 - Core formation and migration

Plasmas problems
2002 - Field lines, magnetic stuff * 
2003 - EM dispersion..?
2005 - Conducting sphere in a collisionless Maxwellian plasma; momentum equation
2006 - force-free * 
2007 - moments of the Vlasov equation, Langmuir dispersion, linearization * 

"Radiative" problems
1999 - Blackbodies, albedo
2003 - Ionization, blackbodies, recombination
2008 - "N slab model" surface temperatures (the solutions kind of suck, I gave up on this question after looking at the solution to the first part which was incomprehensible what they were doing)

"Stellar structure" problems
1994 - Hydrostatic equilibrium and stellar interiors
1995 - Luminosity and effective temperature
2004 - Bondi accretion *
2007 - Stellar interiors... structure and stuff