# README #

This is a place for us to share our knowledge (or lack thereof) of core classes so we can all happily pass comps.

### Current state of the repo ###

The repo currently contains the following:

* Old exams
* Some old study guides
* 2/3rds of an R&D study guide
* 1/3rd of an AMP study guide
* 2/5ths of a math methods study guide
* Handwritten notes for Fluids, Math Methods, R&D, and AMP (Jeremy's notes -- will have radiation, collisions, etc.  11/13)
* Nils' lecture notes from Obs & Stats.


### What is this repository for? ###

* Reviewing core courses!
* Creating Review Guides!
* Dragging Nick into the country and beating the shit out of him.

### How do I get set up? ###

* Clone the repo.

### Contribution guidelines ###

* Be anyone other than Tristan

### Who do I talk to? ###

* No one, you don't deserve friends.
