# README #

This is a place for us to share our knowledge (or lack thereof) of core classes so we can all happily pass comps.

### Current state of the repo ###

The repo currently contains the following:

* Old exams (Up to '18)
* Some old study guides
* AMP: a full '14 study guide (but I dunno who'd use Mike's guide...)
* AMP: a full '15 copy of Jeremy's notes
* Math Methods: A rough draft of the '14 class, which should be just like the '15 class (but will not resemble Math Methods taught by anyone other than Ben Brown)
* Obs & Stats: A copy of Nils' notes 
* Fluids: A rough guide
* R&D: 2/3 of the '15 guide
* Handwritten notes for Fluids, Math Methods, R&D, and AMP from '14-'15



### What is this repository for? ###

* Reviewing core courses!
* Creating Review Guides!

### How do I get set up? ###

* Clone the repo.

### Contribution guidelines ###

* Don't be a bad person.